# ahau-graphql-client

## Example Usage

```js
const AhauClient = require('ahau-graphql-client')

const client = new AhauClient(uri, opts)

// client.query(...)
```

## API

### `new AhauClient(uri, opts) => client`

where:
- `uri` *String* is the http endpoint of your graphql server
    - if *Number*, it's assumed to be a port, and uri will be set to `http://localhost:${port}/graphql`
- `opts` *Object* (optional) lets you set:
    - `opts.possibleTypes` *Object* for [resolving fragments](https://www.apollographql.com/docs/react/data/fragments/)
    - `opts.fetch` *Function* for providing a custom fetch function. This is required when not in the browser
    - `opts.apolloClient` *Object* for setting or over-riding other apollo client options

and returns `client` - an `@apollo/client` instance

